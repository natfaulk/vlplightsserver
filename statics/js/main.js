var app = angular.module('myApp', [])
app.controller('lights', ['$scope', '$http', function($s, $http) {
  console.log("hi")
  $s.test = () => {
    // var rect = canvas.parentNode.getBoundingClientRect();
    // canvas.width = rect.width;
    // canvas.height = rect.height;
    console.log($s.d.c.parentNode.getBoundingClientRect().width)
  }

  $s.page = 1

  $s.filter = ''

  $s.lights = {}
  // get list of devices from main server (vlp.natfaulk.com)
  $http.get('get_ips')
  .then((response) => {
    $s.lights = response.data
    console.log('Received device list')

    $s.getsettings()
  }, (response) => {
    console.log('ERROR, did not receive device list')
    console.log(response)      
  })

  $s.recs = []
  // get list of receivers from main server (vlp.natfaulk.com)
  $http.get('get_ips2')
  .then((response) => {
    // $s.recs = response.data
    console.log('Received receiver list')
    $s.recs = Object.keys(response.data).map(x => {
      let t = response.data[x]
      t.hwid = x
      t.timestamppretty = moment(t.timestamp).fromNow()
      return t
    })
    console.log('Recs', $s.recs)
    $s.recs.sort((a,b) => {
      if (a.hostname < b.hostname)
        return -1;
      if (a.hostname > b.hostname)
        return 1;
      return 0;
    })
  }, (response) => {
    console.log('ERROR, did not receive receiver list')
    console.log(response)      
  })


  $s.setenable = (_hwid, _led, _state) => {
    let config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    let data = `${_led}=${_state}`
    $http.post(`http://${$s.lights[_hwid].ip}/setenabled`, data, config)
    .then((response) => {
      // success
      if (response.data == 'ack')
      {
        $s.lights[_hwid][_led].state = _state;
        console.log('Post successful')
      } else {
        console.log('Post failed')
        console.log(response)
      }
    }, (response) => {
      // failure
      console.log('Post failed')
      console.log(response)
    })
  }

  // need to wait for ng repeat to finish before initialising the canvas 
  // otherwise trying to initialise non existant canvas
  $s.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
    for (const key of Object.keys($s.lights)) {
      console.log(key, $s.lights[key])
      $s.lights[key].fftData = []
      $s.lights[key].d = new Mindrawingjs()
      $s.lights[key].d.setup(`fft${key}`)
    }
  });

  $s.updateFFT = (_hwid) => {
    $http.get(`http://${$s.lights[_hwid].ip}/getfft`)
    .then((response) => {
      $s.lights[_hwid].fftData = response.data
      console.log('Received updated FFT data')
      $s.lights[_hwid].fftData = $s.lights[_hwid].fftData.slice(0, ($s.lights[_hwid].fftData.length) / 2)
      for (let i = 0; i < $s.lights[_hwid].fftData.length; i++) {
        $s.lights[_hwid].fftData[i] = 20 * Math.log10($s.lights[_hwid].fftData[i])
      }
      $s.drawFFT(_hwid)
      // console.log($s.fftData)
    }, (response) => {
      console.log('ERROR, did not receive updated FFT data')
      console.log(response)      
    })
  }

  $s.drawFFT = (_hwid) => {
    let tFFT = $s.lights[_hwid].fftData  
    var rect = $s.lights[_hwid].d.c.parentNode.getBoundingClientRect()
    $s.lights[_hwid].d.c.width = rect.width
    $s.lights[_hwid].d.c.height = rect.height
    let h = $s.lights[_hwid].d.c.height
    let w = $s.lights[_hwid].d.c.width
    $s.lights[_hwid].d.stroke('#000')
    $s.lights[_hwid].d.strokeWeight(1)
    let t_x1 = 0
    let t_x2 = 0
    let t_y1 = 0
    let t_y2 = 0
    let maxValue = 0
    for (let i = 0; i < tFFT.length; i++) {
      if (tFFT[i] > maxValue) maxValue = tFFT[i]
    }
    for (let i = 0; i < tFFT.length - 1; i++) {
      t_x1 = Math.round(w * i / tFFT.length)
      t_x2 = Math.round(w * (i + 1) / tFFT.length)
      t_y1 = Math.round((h - 1) - (tFFT[i] / maxValue) * h)
      t_y2 = Math.round((h - 1) - (tFFT[i + 1] / maxValue) * h)
      $s.lights[_hwid].d.line(t_x1, t_y1, t_x2, t_y2)
    }
  }

  $s.setfreq = (_hwid, _led) => {
    let config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    let data = `${_led}=${$s.lights[_hwid][_led].freq}`
    $http.post(`http://${$s.lights[_hwid].ip}/setfreq`, data, config)
    .then((response) => {
      // success
      if (response.data == 'ack')
      {
        console.log('Set freq successful')
      } else {
        console.log('Set freq failed')
        console.log(response)
      }
    }, (response) => {
      // failure
      console.log('Set freq failed')
      console.log(response)
    })
  }

  $s.setbias = (_hwid) => {
    let config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    let data = `res=${$s.lights[_hwid].biasr}`
    $http.post(`http://${$s.lights[_hwid].ip}/setbiasres`, data, config)
    .then((response) => {
      // success
      if (response.data == 'ack')
      {
        console.log('Set bias successful')
      } else {
        console.log('Set bias failed')
        console.log(response)
      }
    }, (response) => {
      // failure
      console.log('Set bias failed')
      console.log(response)
    })
  }

  $s.setsignal = (_hwid) => {
    let config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    let data = `res=${$s.lights[_hwid].signalr}`
    $http.post(`http://${$s.lights[_hwid].ip}/setsigres`, data, config)
    .then((response) => {
      // success
      if (response.data == 'ack')
      {
        console.log('Set signal res successful')
      } else {
        console.log('Set signal res failed')
        console.log(response)
      }
    }, (response) => {
      // failure
      console.log('Set signal res failed')
      console.log(response)
    })
  }

  $s.getsettings = () => {
    // try contact each device and get settings from them
    // will only work if on same network
    for (const key of Object.keys($s.lights)) {
      //  console.log(key, $s.lights[key])
      $http.get(`http://${$s.lights[key].ip}/getsettings`)
      .then((response) => {
        Object.assign($s.lights[key], response.data)
        console.log(`Successfully got settings from ${$s.lights[key].hostname} at ${$s.lights[key].ip}`)
        $s.updateFFT(key)
      }, (response) => {
        console.log(`No response from ${$s.lights[key].hostname} at ${$s.lights[key].ip}`
        , 'Likely on a different network, so this is not necessarily a problem')
        console.log(response)      
      })
    }
  }

  $s.loadpresets = () => {
    // console.log($s.lights)
    $http.get('get_presets')
    .then((response) => {
      let r = response.data
      // console.log(response.data)
      $s.presetstatus = 'Presets successfully set'
      Object.keys(r).forEach((key) => {
        let hwid = $s.getHwidFromName(key)
        if (hwid !== '') {
          $s.lights[hwid].biasr = r[key].biasr
          $s.lights[hwid].signalr = r[key].signalr
          $s.lights[hwid].ledA = r[key].ledA
          $s.lights[hwid].ledB = r[key].ledB
          $s.lights[hwid].ledC = r[key].ledC
          $s.setbias(hwid)
          $s.setsignal(hwid)
          $s.setenable(hwid, 'ledA', $s.lights[hwid].ledA.state)
          $s.setenable(hwid, 'ledB', $s.lights[hwid].ledB.state)
          $s.setenable(hwid, 'ledC', $s.lights[hwid].ledC.state)
          $s.setfreq(hwid, 'ledA')
          $s.setfreq(hwid, 'ledB')
          $s.setfreq(hwid, 'ledC')
        }
      })
    }, (response) => {
      console.log('no presets found')
      console.log(response)
      $s.presetstatus = 'Error setting presets'
    })
  }

  $s.getHwidFromName = (_hostname) => {
    let output = ''
    Object.keys($s.lights).forEach((key) => {
      if ($s.lights[key].hostname == _hostname) {
        output = key
        return
      }
    })
    return output
  }
}])

app.directive('onFinishRender',['$timeout', '$parse', function ($timeout, $parse) {
  return {
      restrict: 'A',
      link: function (scope, element, attr) {
          if (scope.$last === true) {
              $timeout(function () {
                  scope.$emit('ngRepeatFinished');
                  if(!!attr.onFinishRender){
                    $parse(attr.onFinishRender)(scope);
                  }
              });
          }
      }
  }
}])