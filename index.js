const express = require('express')
const path = require('path')
const fs = require('fs')

const app = express()
let PORT = 3000;
const SAVED_IP_FILE = 'saved_ips.json'
const SAVED_IP_FILE2 = 'saved_ips2.json'

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
  extended: true
}))

let pc_ip = ''

let ipList = {}
let ipList2 = {}

let writeIp2ToDisk = () => {
  fs.writeFile(SAVED_IP_FILE2, JSON.stringify(ipList2), 'utf8', function (err) {
    if (err) console.log(err);
    else console.log("The file was saved!");
  })
}

app.use('/static', express.static('statics'))

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'templates', 'index.html'))
})

app.get('/get_ips', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(ipList))
})

app.get('/get_ips2', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(ipList2))
})

app.get('/clear_ips2', (req, res) => {
  ipList2 = {}
  writeIp2ToDisk()
  res.send('ack')
})

app.get('/get_presets', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  fs.readFile('preset.json', 'utf8', function (err, data) {
    if (err) {
      res.send('{}')
    } else {
      res.send(data)
    }
  })
})

app.post('/set_pc_ip', (req, res) => {
  if (req.body.ip) {
    pc_ip = req.body.ip
    res.send('ack')
  } else res.send('nack')
})

app.get('/get_pc_ip', (req, res) => {
  res.send(pc_ip)
})

app.post('/set_ip', (req, res) => {
  if (req.body.ip && req.body.hostname && req.body.hwid)
  {
    ipList[req.body.hwid] = {
      "hostname": req.body.hostname,
      "ip": req.body.ip,
      "timestamp": new Date().getTime()
    }
    res.send('ack')
    fs.writeFile(SAVED_IP_FILE, JSON.stringify(ipList), 'utf8', function (err) {
      if (err) console.log(err);
      else console.log("The file was saved!");
    })
  } else {
    res.send('nack')
  }
})

app.post('/set_ip2', (req, res) => {
  if (req.body.ip && req.body.hostname && req.body.hwid && req.body.type && req.body.version)
  {
    ipList2[req.body.hwid] = {
      "hostname": req.body.hostname,
      "ip": req.body.ip,
      "timestamp": new Date().getTime(),
      "type": req.body.type,
      "version": req.body.version
    }
    res.send('ack')
    writeIp2ToDisk()
  } else {
    res.send('nack')
  }
})

fs.readFile('settings.json', 'utf8', function (err, data) {
  if (err)
  {
    console.log('No settings file, using port 3000')
  } else {
    let settings = JSON.parse(data)
    PORT = settings.port
  }
  
  fs.readFile(SAVED_IP_FILE, 'utf8', function (err, data) {
    if (err)
    {
      console.log('No saved ip file')
    } else {
      ipList = JSON.parse(data)
    }

    fs.readFile(SAVED_IP_FILE2, 'utf8', function (err, data) {
      if (err)
      {
        console.log('No saved ip file 2')
      } else {
        ipList2 = JSON.parse(data)
      }
      
      app.listen(PORT, () => {
        console.log(`Example app listening on port ${PORT}!`)
      })
    })
  })
})

// let allowCrossDomain = function(req, res, next) {
//   res.header('Access-Control-Allow-Origin', "*")
//   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
//   res.header('Access-Control-Allow-Headers', 'Content-Type')
//   next()
// }

// app.use(allowCrossDomain)